//
//  ViewController.m
//  show items
//
//  Created by Click Labs133 on 9/29/15.
//  Copyright (c) 2015 vijay kumar. All rights reserved.
//

#import "ViewController.h"
NSMutableArray *tableData;
@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UIButton *button;

@end

@implementation ViewController
@synthesize tableView;
@synthesize textField;
@synthesize button;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
   tableData=[[NSMutableArray alloc]init];
   
    [button addTarget:self action:@selector(press_me:) forControlEvents:UIControlEventTouchUpInside];
    
    
}
-(void)press_me:(UIButton *)sender{
    [tableData addObject:textField.text];
    [tableView reloadData];
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [tableData count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell1"];
    cell.textLabel.text=tableData[indexPath.row];
    return cell;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
